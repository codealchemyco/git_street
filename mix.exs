defmodule GitStreet.MixProject do
  use Mix.Project

  def project do
    [
      app: :git_street,
      version: "0.1.4",
      elixir: "~> 1.0",
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      name: "GitStreet",
      source_url: "https://gitlab.com/codealchemyco/git_street"
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:ex_doc, "~> 0.14", only: :dev, runtime: false}
    ]
  end

  defp description() do
    "A simple wrapper of git CLI for Elixir."
  end

  defp package() do
    [
      name: "git_street",
      licenses: ["Apache-2.0"],
      links: %{"GitLab" => "https://gitlab.com/codealchemyco/git_street"}
    ]
  end
end
